#include "position.h"
#include "portfolio.h"
#include "transaction.h"


void EquityTransaction::perform(int amount, bool reversing) const {
    auto pos = portfolio_->getPosition(name_);

    if (!pos) {
        pos = std::make_shared<EquityPosition>(
            std::make_shared<Equity>(name_), amount, price_);
    } else if (reversing) {
        pos->regressPosition(amount, price_);
    } else {
        pos->progressPosition(amount, price_);
    }
    portfolio_->handleModifiedPosition(pos);
}

void EquityTransaction::execute() const {
    perform(amount_, false);
}

void EquityTransaction::reverse() const {
    perform(amount_, true);
}

void EquityTransaction::print(std::ostream& out) const {
    out << name_ << ": " << amount_ << " @ " << price_;
}
