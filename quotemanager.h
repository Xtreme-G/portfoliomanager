#pragma once

#include <map>

#include "quotemanager.h"
#include "singleton.h"

class QuoteManager final : public Singleton<QuoteManager> {
  public:
    double getQuote(const std::string& name) const { return quoteMap_.at(name); }
    void setQuote(const std::string& name, double quote) { quoteMap_[name] = quote; }

  private:
    std::map<std::string, double> quoteMap_;
};