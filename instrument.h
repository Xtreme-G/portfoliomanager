#pragma once

#include <string>

#include "quotemanager.h"

class Instrument {
  public:
    Instrument(const std::string& name)
    : name_(name) {};
    virtual double NPV() const = 0;
    const std::string& name() const { return name_; }
  private:
    std::string name_;
};


class Equity : public Instrument {
  public:
    Equity(const std::string& name)
    : Instrument(name) {};
    double NPV() const override { 
        return QuoteManager::instance().getQuote(name()); 
    }
};