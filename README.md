## Design Philsophy 

The portfolio manager is based on **Transaction**s, which are owned by the **Portfolio**. The only way to operate on the portfolio should be to execute transactions, which modifies the **Portfolio**'s **Positions**. The **Portfolio** is a **TransactionManager**, which enables it to roll back the view of the **Position**s to a specific date by executing and revering the **Transaction**s preceeding that date. 

The portfolio manager can be extended by adding additional **Instrument** and **Transaction** types.

---

## Input

The file Transaktions.dat is used as input.

---

## Output

The program will output the **Portfolio**'s positions at 2007-12-31 to positions.txt and value the **Portfolio** assets at 2008-02-28.