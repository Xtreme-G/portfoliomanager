#include <chrono>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <regex>
#include <sstream>
#include <vector>
#include <stdexcept>

#include "position.h"
#include "portfolio.h"
#include "quotemanager.h"
#include "transaction.h"

using Transactions = std::vector<std::unique_ptr<Transaction>>;

Date get_date_from_string(const std::string& str, 
                          std::string& format = std::string("%Y-%m-%d")) {
    std::tm tm = {};
    std::stringstream ss(str);
    ss >> std::get_time(&tm, format.c_str());
    if (ss.fail())
        throw std::runtime_error("Could not open parse date: " 
                                 + str + " using format:" + format);
    return std::chrono::system_clock::from_time_t(std::mktime(&tm));
}

Transactions parseTransactions(const std::string& fileName) {
    Transactions transactions;
    std::ifstream file;
    file.open(fileName);
    if (!file.is_open())
        throw std::runtime_error("Could not open file");

    std::string content, name, cost, date, numb;
    while (std::getline(file, content)) {
        name = content.substr(0, 7);
        cost = content.substr(7, 6);
        date = content.substr(13, 8);
        numb = content.substr(21, std::string::npos);

        // Remove trailing whitepace
        name = std::regex_replace(name, std::regex("[ \t]+$"), "");

        // There is a bug in MSVC when parsing a date without separators
        date.insert(4, 1, '-');
        date.insert(7, 1, '-');
        auto t = get_date_from_string(date);

        int number = std::stoi(numb);
        double price = std::stod(cost) / std::abs(number);

        transactions.emplace_back(std::make_unique<EquityTransaction>(
            t, name, price, number));
    }
    return transactions;
}


int main() {

    Portfolio portfolio;
    Transactions transactions;

    // Part I

    try {
        transactions = parseTransactions("Transaktions.dat");
    } catch (std::exception &ex) { 
        std::cout << ex.what() << std::endl;
        return 1;
    }
    
    // Let the portfolio execute and take ownership of transactions
    for (auto&& tr : transactions)
        portfolio.executeTransaction(std::move(tr));

    portfolio.rollBackToDate(get_date_from_string("2007-12-31"));

    // Write positions to file
    {
        std::ofstream file("positions.txt");
        for (const auto& pair : portfolio.getActivePositions())
            file << *pair.second << std::endl;
    }
    
    // Part II
    portfolio.rollBackToDate(get_date_from_string("2008-02-28"));

    QuoteManager::instance().setQuote("ABB LTD", 159.50);
    QuoteManager::instance().setQuote("VOLVO B",  93.75);
    QuoteManager::instance().setQuote("SSAB B",  160.50);

    // Write positions to file
    {
        std::ofstream file("values.txt");
        for (const auto& pair : portfolio.getActivePositions()) {
            auto pos = pair.second;
            file << pos->getInstrument()->name() << ";" 
                 << pos->amount() << ";" << pos->NPV() << std::endl;
        }
    }

    return 0;
}