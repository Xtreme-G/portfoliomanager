#include "position.h"

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

void Position::progressPosition(int diff, double price) {
    amount_ += diff;
}

void Position::regressPosition(int diff, double price) {
    amount_ -= diff;
}

void EquityPosition::print(std::ostream& out) const {
    out << getInstrument()->name() << ";" << amount() << ";" << GAV();
}

void EquityPosition::progressPosition(int diff, double price) {

    /* Three cases:
       1. the amount is switching sign: reset GAV
       2. position is increasing in absolute amount: adjust GAV
       3. position is decreasing in absolute amount: GAV stays the same
    */
    if (std::abs(diff) > std::abs(amount()) &&
        sgn(diff) != sgn(amount()))
        GAV_ = price;
    else if (sgn(amount()) == sgn(diff))
        GAV_ = (GAV_ * amount() + diff * price) / (amount() + diff);

    Position::progressPosition(diff, price);
}

void EquityPosition::regressPosition(int diff, double price) {

    /* Three cases:
    1. the amount is switching sign, reset GAV: reset GAV
    2. position is decreasing in absolute amount: adjust GAV
    3. position is increasing in absolute amount: GAV stays the same 
    */
    if (std::abs(diff) > std::abs(amount()) &&
        sgn(diff) != sgn(amount()))
        GAV_ = price;
    else if (sgn(amount()) == sgn(diff))
        GAV_ = (GAV_ * (amount()) - price * diff) / (amount() - diff);

    Position::regressPosition(diff, price);
}
