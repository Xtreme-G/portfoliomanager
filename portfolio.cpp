#include <iostream>
#include <memory>

#include "portfolio.h"
#include "position.h"

void Portfolio::executeTransaction(std::unique_ptr<Transaction> transaction) {
    rollBackToDate(transaction->date());
    transaction->setPortfolio(this);
    execute(std::move(transaction));
}

std::shared_ptr<Position> Portfolio::getPosition(const std::string& name) const {
    auto pos = getActivePosition(name);
    if (!pos)
        return getClosedPosition(name);
    return pos;
}

void Portfolio::handleModifiedPosition(std::shared_ptr<Position> pos) {
    auto name = pos->getInstrument()->name();
    if (auto activePos = getActivePosition(name)) {
        if (activePos->amount() == 0)
            closePosition(activePos);
    } else if (auto closedPos = getClosedPosition(name)) {
        activatePosition(closedPos);
    } else {
        openPosition(pos);
    }
}

std::shared_ptr<Position> Portfolio::getActivePosition(const std::string& name) const {
    auto it = activePositions_.find(name);
    return it == activePositions_.end() ? nullptr : it->second;
}

std::shared_ptr<Position> Portfolio::getClosedPosition(const std::string& name) const {
    auto it = activePositions_.find(name);
    return it == activePositions_.end() ? nullptr : it->second;
}

void Portfolio::openPosition(std::shared_ptr<Position> pos) {
    activePositions_[pos->getInstrument()->name()] = pos;
}

void Portfolio::closePosition(std::shared_ptr<Position> pos) {
    activePositions_.erase(pos->getInstrument()->name());
    closedPositions_[pos->getInstrument()->name()] = pos;
}

void Portfolio::activatePosition(std::shared_ptr<Position> pos) {
    closedPositions_.erase(pos->getInstrument()->name());
    activePositions_[pos->getInstrument()->name()] = pos;
}




