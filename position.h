#pragma once

#include <memory>

#include "instrument.h"

class Position {
  public:
    Position(std::shared_ptr<Instrument> instrument,
             int amount)
    : instrument_(instrument)
    , amount_(amount) {};
    virtual ~Position() {};
    virtual double NPV() const = 0;
    virtual void progressPosition(int diff, double price);
    virtual void regressPosition(int diff, double price);
    virtual void print(std::ostream& out) const = 0;
    std::shared_ptr<Instrument> getInstrument() const { return instrument_; }
    int amount() const { return amount_; }

  private:
    std::shared_ptr<Instrument> instrument_;
    int amount_;
};

inline std::ostream& operator<< (std::ostream& os, const Position& pos) {
    pos.print(os);
    return os;
}


class EquityPosition : public Position {
  public:
    EquityPosition(std::shared_ptr<Equity> equity, int amount, double GAV)
    : Position(equity, amount)
    , GAV_(GAV) {};

    double NPV() const override { return amount() * getInstrument()->NPV(); }
    double GAV() const { return GAV_; }
    void print(std::ostream& out) const override;

  private:

    friend class EquityTransaction;

    void progressPosition(int diff, double price) override;
    void regressPosition(int diff, double price) override;

    double GAV_;
};