#pragma once

#include <chrono>
#include <memory>
#include <string>
#include <vector>


class Portfolio;
using Date = std::chrono::system_clock::time_point;

class Transaction {
  public:

    Transaction(const Date& date) 
    : date_(date) {};

    virtual ~Transaction() {};
    virtual void execute() const = 0;
    virtual void reverse() const = 0;
    Date date() const { return date_; }
    virtual void print(std::ostream& out) const = 0;

  protected:
    Date date_;
    Portfolio* portfolio_;

  private:
    friend class Portfolio;
    void setPortfolio(Portfolio* portfolio) { portfolio_ = portfolio; }
};

inline std::ostream& operator<< (std::ostream& os, const Transaction& transaction) {
    transaction.print(os);
    return os;
}

inline bool operator<(const Transaction& lhs, const Transaction& rhs) {
    return lhs.date() < rhs.date();
}


class EquityTransaction : public Transaction {
  public:
    EquityTransaction(const Date& date,
                      const std::string name,
                      double price, 
                      int amount)
    : Transaction(date)
    , name_(name)
    , price_(price)
    , amount_(amount) {};
    
    void execute() const override;
    void reverse() const override;

    void print(std::ostream& out) const override;
  
  private:
    void perform(int amount, bool reversing) const;

    std::string name_;
    double price_;
    int amount_;
};

