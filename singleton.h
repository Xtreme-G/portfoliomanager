#pragma once

// https://codereview.stackexchange.com/questions/173929/modern-c-singleton-template

template<typename T>
class Singleton {
  public:
    static T& instance() {
        // Since it's a static variable, if the class has already been created,
        // it won't be created again.
        // And it **is** thread-safe in C++11.
        static T instance_;

        // Return a reference to our instance.
        return instance_;
    }

    // delete copy and move constructors and assign operators
    Singleton(const Singleton&) = delete;             // Copy construct
    Singleton(Singleton&&) = delete;                  // Move construct
    Singleton& operator=(const Singleton&) = delete;  // Copy assign
    Singleton& operator=(Singleton&&) = delete;       // Move assign

  protected:
    Singleton() {}
    ~Singleton() {}
};