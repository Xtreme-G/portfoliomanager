#include "portfolio.h"
#include "transaction.h"
#include "transactionmanager.h"

TransactionManager::TransactionManager()
    : replicationDate_(Date::min()) {}

void TransactionManager::execute(std::unique_ptr<Transaction> transaction) {
    transaction->execute();
    executedTransactions_.push_back(std::move(transaction));
}

void TransactionManager::progress() {
    reversedTransactions_.top()->execute();  // progress most recently executed command.
    moveForward();
}

void TransactionManager::reverse() {
    executedTransactions_.back()->reverse();  // reverse most recently executed command.
    moveBackward();
}

void TransactionManager::moveForward() {
    // add reversed transaction to reverse container.
    executedTransactions_.push_back(std::move(reversedTransactions_.top()));
    reversedTransactions_.pop();  // remove front entry from reverse container.
}

void TransactionManager::moveBackward() {
    // add executed transaction to progress container.
    reversedTransactions_.push(std::move(executedTransactions_.back()));
    executedTransactions_.pop_back();  // remove front entry from progress container.
}

const std::unique_ptr<Transaction>& TransactionManager::latestExecuted() const {
    return executedTransactions_.back();
}

const std::unique_ptr<Transaction>& TransactionManager::latestReversed() const {
    return reversedTransactions_.top();
}

void TransactionManager::rollBackToDate(const Date& replicationDate) {
    if (replicationDate_ == replicationDate)
        return;
    // reverse transactions while replication date is after most recent transaction.
    while (reversible() && latestExecuted()->date() > replicationDate)
        reverse();
    // execute transactions while replication date is before most recent transaction.
    while (progressable() && latestReversed()->date() <= replicationDate)
        progress();
    replicationDate_ = replicationDate;
}
 
