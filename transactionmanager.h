#pragma once

#include <chrono>
#include <stack>
#include <vector>

#include "transaction.h"

class Portfolio;

class TransactionManager {
  public:
    using Transactions = std::vector<std::unique_ptr<Transaction>>;
    using Date = std::chrono::system_clock::time_point;

    TransactionManager();
    ~TransactionManager() {};

    Date replicationDate() const { return replicationDate_; };
    const std::unique_ptr<Transaction>& latestExecuted() const;
    const std::unique_ptr<Transaction>& latestReversed() const;
    void rollBackToDate(const Date& replicationDate);

  protected:

    void progress();
    void reverse();
    void moveForward();
    void moveBackward();

    void execute(std::unique_ptr<Transaction>);
    bool reversible() const { return !executedTransactions_.empty(); }
    bool progressable() const { return !reversedTransactions_.empty(); }

  private:
    std::stack<std::unique_ptr<Transaction>> reversedTransactions_;
    std::vector<std::unique_ptr<Transaction>> executedTransactions_;
    Date replicationDate_;
};