#pragma once

#include <memory>
#include <string>
#include <unordered_map>

#include "transactionmanager.h"

class Position;
class Transaction;

using Positions = std::unordered_map<std::string, std::shared_ptr<Position>>;

class Portfolio : public TransactionManager {

  public:
    double NPV() const;
    void executeTransaction(std::unique_ptr<Transaction> transaction);
    const Positions& getActivePositions() const { return activePositions_; }
    const Positions& getClosedPositions() const { return closedPositions_; }

  private:

    friend class EquityTransaction;

    std::shared_ptr<Position> getPosition(const std::string& name) const;
    std::shared_ptr<Position> getActivePosition(const std::string& name) const;
    std::shared_ptr<Position> getClosedPosition(const std::string& name) const;

    void openPosition(std::shared_ptr<Position> pos);
    void closePosition(std::shared_ptr<Position> pos);
    void activatePosition(std::shared_ptr<Position> pos);
    void handleModifiedPosition(std::shared_ptr<Position> pos);

    Positions activePositions_;
    Positions closedPositions_;
};